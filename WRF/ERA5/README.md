# Downloading ERA5 for running WRF via cds.climate.copernicus.eu (check: https://cds.climate.copernicus.eu/api-how-to):

1. Create an accout at https://cds.climate.copernicus.eu

2. Install the CDS API key, in file $HOME/.cdsapirc put these 2 lines:
	url: https://cds.climate.copernicus.eu/api/v2
	key: <apt key> 	# check https://cds.climate.copernicus.eu/api-how-to 

3. Set python enviroment using conda

4. Install "cdsapi":
	pip install cdsapi

5. run ./DownloadERA5_pl.sh to download data on pressure levels 
	(https://dreambooker.site/2019/10/03/Initializing-the-WRF-model-with-ERA5-pressure-level/)
   run ./DownloadERA5_ml.sh to download data on model levels    
   	(https://dreambooker.site/2018/04/20/Initializing-the-WRF-model-with-ERA5/)

	

