#!/bin/bash
#SBATCH --job-name=WRF_WPS
#SBATCH --output=WRF_WPS%j.out
#SBATCH --error=WRF_WPS%j.out
#SBATCH --ntasks=1
#SBATCH --ntasks-per-node=1
#SBATCH --time=01:00:00
#SBATCH --mail-user=<email>
#SBATCH --reservation=meteo_16

# Memory extension
ulimit -s unlimited

# What to do
export COMPILE_WRF='false'
export COMPILE_WPS='true'

# Define INTEL version that you want to use
export INTEL="2020" # "2020"

# To use locally installed libraries
module use /gpfs/projects/meteo/WORK/ASNA/apps/privatemodules  # set path to locally installed libraries

# Load libraries
module purge
if [ ${INTEL} = "2013" ] ; then
	echo "Compiling with Intel version 2013"
	module purge
	module load INTEL/PSXE2013u1
	module load openmpi/4.0.0-Intel13_UCX				# locally installed
	module load wrf_lib_2019/wrflib_ompi4.0.0_Intel13	# locally installed
elif [ ${INTEL} = "2020" ] ; then
	echo "Compiling with Intel version 2020"
	module purge
	module load INTEL/2020                              	
	module load OPENMPI/4.1.0               		
	module load wrflibs/i2020_ompi410					# locally installed       		
	source /gpfs/res_apps/INTEL/2020/bin/compilervars.sh intel64
else
        echo "Version of INTEL not defined or not available on Altamira, exiting ..."
        exit 1
fi


# Set folder path to a working directory
export WORKDIR=`pwd`

# Set enviromental variables
export CC=icc
export CXX=icpc
export FC=ifort
export F77=ifort
export F90=ifort
export NETCDF4=1
export WRFIO_NCD_LARGE_FILE_SUPPORT=1

############################################################################################
#                               Compile WRF                                      
############################################################################################
if [ ${COMPILE_WRF} = 'true' ]; then

# Compile in serial ("-j 1") or in parallel ("-j n" , n > 1). Wrf defalut is "-j 2"
export J="-j 1"         

cd $WORKDIR

# Download WRF
wget https://github.com/wrf-model/WRF/archive/v4.2.2.tar.gz
tar xvf v4.2.2.tar.gz
cd WRF-4.2.2/

# Configure WRF
./clean -a
./configure > configure.log << EOF
15
1
EOF

# Manual changes in configure.wrf file
# 1. Enable GHG 
sed -i -e 's/-DNETCDF/-DNETCDF -DCLWRFGHG/' configure.wrf
# 2. Exclude CLM LSM option, not necessary but prolongs the compilation
sed -i -e 's/-DWRF_USE_CLM//' configure.wrf
# 3. Fix compile options for INTEL/2020
if [ ${INTEL} = "2020" ] ; then
	sed -i -e 's/mpicc -cc=$(SCC)/mpicc/' configure.wrf
	sed -i -e 's/$(DM_CC) -DFSEEKO64_OK/$(DM_CC)/' configure.wrf
	sed -i -e 's/$(NETCDF4_IO_OPTS) -DRPC_TYPES=1/$(NETCDF4_IO_OPTS)/' configure.wrf
fi

# Compile WRF
./compile em_real 2>&1 | tee compile.log
fi

############################################################################################
#                               Compile WPS                                      
############################################################################################
if [ ${COMPILE_WPS} = 'true' ]; then

# Compile in serial ("-j 1") or in parallel ("-j n" , n > 1). Wrf defalut is "-j 2"
export J="-j 1"

cd $WORKDIR

# Download WPS
wget https://github.com/wrf-model/WPS/archive/v4.2.tar.gz
tar xvf v4.2.tar.gz
cd WPS-4.2/

# Configure WPS
./clean -a
./configure > configure.log << EOF
17
EOF

# Fix make file for metgrid and geodgrid to have a succesful compilation
sed -i -e 's/$(MPI_LIB)/ /' geogrid/src/Makefile
sed -i -e 's/$(MPI_LIB)/ /' metgrid/src/Makefile

# Compile WPS
./compile 2>&1 | tee compile.log
fi


